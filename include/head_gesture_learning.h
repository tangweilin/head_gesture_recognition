#ifndef HEAD_GESTURE_LEARNING_H
#define HEAD_GESTURE_LEARNING_H

#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include "opencv2/opencv.hpp"
#include "CvHMM.h"

using namespace std;
using namespace cv;

class HeadGestureLearning
{
public:
	HeadGestureLearning();
	HeadGestureLearning(const Mat &init_trans, const Mat &init_emis, const Mat &init_pi);
	~HeadGestureLearning() {}

	/* data */
	void fetch_data(const char*);   // obtain sample sequence from file
	void fetch_data(const Mat&);    // obtain sample sequence directly by assign

	Mat  get_trans();
	Mat  get_emis();
	Mat  get_pi(); 
	Mat  get_seq();

	/* train */
	void hmm_train(const CvHMM &hmm_model, 
				   const int max_iter
				  );  // train samples for parameters

	/* display */
	void display();

private:
	std::vector<Mat> m_seq;
	Mat m_trans;
	Mat m_emis;
	Mat m_pi;
};


#endif
