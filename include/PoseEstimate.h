#ifndef POSE_ESTIMATE_H_
#define POSE_ESTIMATE_H_

#include <stdio.h>
#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>
#include "comdef.h"

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	/* Description: runing head pose estimate, return 3 degree(roll, pitch ,yaw)
	 * Input: inputLandmarks, the landmarks of face
	 * Output: outputPose, head pose of face 	
	 * Return: -1 means error, 0 means succcess
	 */
	int RunPoseEstimate(cv::Mat &inputLandmarks, cv::Mat &outputPose);


#ifdef __cplusplus
}
#endif

#endif

