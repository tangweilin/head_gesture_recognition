#ifndef SDMLIB_H
#define SDMLIB_H

#include <stdio.h>
#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>
#include "comdef.h"

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	/* Description: initialize the face alignment sdm engine
	 * Input: sdEngine, face alignment engine	
	 * Output: sdEngine, face alignment engine	
	 * Return: -1 means error, 0 means succcess
	 */
	int InitFaceAlignmentSDM(SDEngine* pSDEngine);


	/* Description: runing face alignment, return 51 face landmarks
	 * Input: inputImage, image to process, MUST BE GRAY
	 *        sdEngine, face alignment engine
	 *        inputFaceRectangle, face detection rectangle
	 * Output: outputFaceLandmarks, face landmarks coordinates	
	 * Return: -1 means error, 0 means succcess
	 */
	int RunFaceAlignmentSDM(SDEngine* pSDEngine, cv::Mat &inputImage, cv::Mat &inputFaceRectangle, cv::Mat &outputFaceLandmarks);


	/* Description: free the face alignment engine
	 * Input: sdEngine, face alignment engine
	 * Output: void
	 * Return: void
	 */
	void FreeFaceAlignmentSDM(SDEngine* pSDEngine);

#ifdef __cplusplus
}
#endif

#endif
