/************************************************************************************

   FileName:   gkumar_quiz.cpp
   Executable: gkumar_quix.exe
   Date:       08-21-2014

   A program to list all the subdirectories and files recursively under a given
   directory

   
   Instruction:
               Compile: g++ gkumar_quiz.cpp -o gkumar_quiz.exe
               Run:     ./gkumar_quiz.exe <pathName> <o for normal output 
                         and 1 for sorted output>

************************************************************************************/

#pragma once   // add by william

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <algorithm>
using namespace std;

/************************************************************************************ 

   Name:        node
   Type:        Structure
   Description: A structure to store files or directory information
   Fields:      name: name of the file or directory
                type: "F" for file and "D" for directory
                size: size of the file, 0 for directory
                time: last modified time in unix format               
************************************************************************************/
struct node
{
  string name;   
  string type; 
  unsigned long long size;
  unsigned long long time;
};

bool compareNode(const node &a, const node &b);
void listdir(const char *name, vector<node> &ans, bool lastSlash);
