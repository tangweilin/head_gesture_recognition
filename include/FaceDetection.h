#ifndef FACE_DETECTION_H_
#define FACE_DETECTION_H_

#include <stdio.h>
#include <stdlib.h>

#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>

using namespace std;

typedef void* FDEngine;

#ifdef __cplusplus
extern "C" {
#endif

	/* Description: initialize the face detection engine
	 * Input: cascadeFile, face detection model file
	 * Output: fdEngine, face detection engine	
	 * Return: -1 means error, 0 means succcess
	 */
	int InitFaceDetection(FDEngine* fdEngine,char *cascadeFile);


	/* Description: runing face detection, only return the biggest face
	 * Input: inputImage, image to process, MUST BE GRAY
	 *        fdEngine, face detection engine
	 * Output: outputFaceRectangle, face detection results	
	 * Return: -1 means error, 0 means succcess
	 */
	int RunFaceDetection(FDEngine* fdEngine, cv::Mat &inputImage, cv::Mat &outputFaceRectangle);

	/* Description: free the face detection engine
	 * Input: fdEngine, face detection engine
	 * Output: void
	 * Return: void
	 */
	void FreeFaceDetection(FDEngine* fdEngine);

#ifdef __cplusplus
}
#endif


#endif
