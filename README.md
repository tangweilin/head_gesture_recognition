# README #

Head Gesture Recognition Project
Create by William Tang

## What is this repository for? ##

* automatic annotation of samples
* HMM model(param) training
* head gesture recognition with HMM

## How to use?  ##
* automatic annotation
> $./hmm_seq_gen shake_seq_dir nod_seq_dir

* head gesture recognition
> $./test_hg test_dir seq_list shake_model nod_model