#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "head_gesture_learning.h"

using namespace boost;

void print(std::vector<string> &v)
{
	for (size_t n = 0; n < v.size(); ++n) {
		cout << v[n] << "\n";
	}
	cout << endl;
}



/* Constructor */
HeadGestureLearning::HeadGestureLearning()
{

}

HeadGestureLearning::HeadGestureLearning(const Mat &init_trans,
										 const Mat &init_emis,
										 const Mat &init_pi)
{
	m_trans = init_trans.clone();
	m_emis  = init_emis.clone();
	m_pi    = init_pi.clone();
}

/**
 * @param path:(I) file to store sequence data
 * @return: void
 * @date  : 2015/12/25
 * @file format
 *  p1:x x x x x x x x x x x x ...
 *  p2:y y y y y y y y y y y y ...
 *  p3:z z z z z z z z z z z z ...
 *  ....
 */
void HeadGestureLearning::fetch_data(const char* path)
{
	ifstream fs(path);
	string line;
	std::vector<int> value;
	std::vector<Mat> samples;

	if (fs.is_open()) {
		std::vector<string> fields;
		std::vector<string> tmp;
		stringstream ss;
		int i;
		while (getline(fs, line)) {
			// cout << line << '\n';
			split(fields, line, is_any_of(" "));
			split(tmp, fields.at(0), is_any_of(":"));

			ss << tmp.at(1);
			ss >> i;
			value.push_back(i);
			ss.clear();

			for (size_t j = 1; j < fields.size()-1; j++) {
				ss << fields.at(j);
				ss >> i;
				value.push_back(i);
				ss.clear();
			}

			// assign value to mat
			Mat tmp_seq(1, value.size(), CV_64F);
			for (size_t j = 0; j < value.size(); j++) {
				tmp_seq.at<int>(0,j) = value.at(j);
			}
			samples.push_back(tmp_seq); 

			fields.clear();
			value.clear();
			tmp.clear();
		}

		// 将样本数据矩阵赋值给成员变量m_seq
		// Mat tmp_seq(samples.size(), value.size(), CV_64F);
		// for (int i = 0; i < tmp_seq.rows; i++) {
		// 	int *ptr = tmp_seq.ptr<int>(i);
		// 	Mat each_row = samples.at(i);
		// 	for (int j = 0; j < tmp_seq.cols; j++) {
		// 		ptr[j] = each_row.at<int>(0,j);
		// 	}
		// }

		// m_seq = tmp_seq.clone();
		m_seq.assign(samples.begin(), samples.end());

		fs.close();
	} else {
		cout << "failed to open file" << endl;
	} 

}


/**
 * @param data:(I) assign input data to member variable
 * @return void
 * 
 */
void HeadGestureLearning::fetch_data(const Mat &data)
{

}


/**
 * Descript: train module for hmm parameters
 * @param hmm_model: (I) HMM Class object
 * @return: void
 * @author: tangwl
 * @date  : 2015/12/28
 *
 */ 
void HeadGestureLearning::hmm_train(const CvHMM &hmm_model,  const int max_iter)
{
	Mat seqs = this->get_seq();
	/*
    for (int i = 0; i < m_trans.rows; i++) {
        for (int j = 0; j < m_trans.cols; j++) {
            cout << m_trans.at<double>(i,j) << " ";
        }
        cout << endl;
    }
    */
	hmm_model.train(seqs, max_iter, m_trans, m_emis, m_pi);
}

/* return data */
Mat HeadGestureLearning::get_trans()
{
	return m_trans;
}

Mat HeadGestureLearning::get_emis()
{
	return m_emis;
}
	
Mat HeadGestureLearning::get_pi()
{
	return m_pi;
}

Mat HeadGestureLearning::get_seq()
{
	Mat seqs = Mat(m_seq.size(), m_seq.at(0).cols, CV_64F);

	for (size_t i = 0; i < m_seq.size(); i++) {
		int *ptr = seqs.ptr<int>(i);
		Mat tmp = m_seq.at(i); 
		for (int j = 0; j < tmp.cols; j++) {
			ptr[j] = tmp.at<int>(0, j);
		}
	}

	return seqs;
}
/**
 * @param  null
 * @return void
 */
void HeadGestureLearning::display()
{
	std::vector<Mat>::iterator iter;
	for (iter = m_seq.begin(); iter != m_seq.end(); iter++) {
		for (int i = 0; i < iter->cols; i++) {
			cout << iter->at<int>(0, i) << " ";
		}
		cout << "\n";
	}
}
