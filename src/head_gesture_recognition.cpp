#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <math.h>
#include <string.h>

#include "FaceDetection.h"
#include "FaceAlignmentSDM.h"
#include "PoseEstimate.h"
#include "head_gesture_learning.h"

#define MAX_FRAMES     200
#define THRESHOLD      -100.0f
#define THRESH_PITCH   2.0f
#define THRESH_YAW     2.0f
#define MAX_ITER       20    // modify max iteration times

using namespace std;
using namespace cv;

// face detection model path
static char cascadeFileName[256] = "./models/haarcascade_frontalface_alt2.xml";

// parse hmm model file and get model parameters
void parse_hmm_model(const char* hmm_model_path, 
	                 Mat &trans_mat, 
	                 Mat &emis_mat
	                );

// Generate an observation sequence according to two adjacent frame
int generate_seq(const vector<float> &pitch_vec, 
	             const vector<float> &yaw_vec, 
	             unsigned long seq_len, 
	             Mat &obs_seq
	            );

// print data
void print_matrix(const Mat &data)
{
	for (int i = 0; i < data.rows; i++) {
		for (int j = 0; j < data.cols; j++) {
			cout << data.at<double>(i,j) << " ";
		}
		cout << endl;
	}
}


int main(int argc, char const *argv[])
{
	if (argc != 5) {
		fprintf(stderr, "usage: exe\timage_dir\timage_list\tshake_model\tnod_model\n" );
	}

	std::vector<Mat> total_frames;   // image lists
	std::vector<Mat> cur_frames;     // current frames
	Mat TRANS, EMIS;                 // transition prob matrix, emission prob matrix
	Mat p_nod_state, p_shake_state;
	Mat nod_fw, nod_bw, shake_fw, shake_bw;
	double nod_logseq, shake_logseq;

	double nod_trans[]   = {0.7, 0.2, 0.1, 
                                0.4, 0.2, 0.4,
                                0.1, 0.2, 0.7};
	double nod_emis[]    = {0, 0, 1, 0, 0, 
                                0, 0, 0, 0, 1, 
                                0, 0, 0, 1, 0};
	double shake_trans[] = {0.6, 0.2, 0.2, 
                                0.4, 0.2, 0.4, 
                                0.2, 0.2, 0.6}; 
	double shake_emis[]  = {1, 0, 0, 0, 0, 
                                0, 0, 0, 0, 1, 
                                0, 1, 0, 0, 0};
	double init_priori[] = {0.4, 0.2, 0.4};

	// parameters 
	Mat nod_transmt   = Mat(3,3,CV_64F,nod_trans).clone();
	Mat nod_emismt    = Mat(3,5,CV_64F,nod_emis).clone();
	Mat nod_pi        = Mat(1,3,CV_64F,init_priori).clone();
	Mat shake_transmt = Mat(3,3,CV_64F,shake_trans).clone();
	Mat shake_emismt  = Mat(3,5,CV_64F,shake_emis).clone();
	Mat shake_pi      = Mat(1,3,CV_64F,init_priori).clone();

	cout << "original shake state prob" << endl;
	print_matrix(shake_transmt);
	cout << "original shake observation prob" << endl;
	print_matrix(shake_emismt);
	cout << "original nod state prob" << endl;
	print_matrix(nod_transmt);
	cout << "original nod observation prob" << endl;
	print_matrix(nod_emismt);

	CvHMM hmm_nod, hmm_shake;

	// do preparation for pose estimation
	FDEngine fdEngine;
	SDEngine sdEngine;

	if (-1 == InitFaceDetection(&fdEngine, cascadeFileName)) {
		fprintf(stderr, "face detection initial failed.\n" );
		return -1;
	}

	if (-1 == InitFaceAlignmentSDM(&sdEngine)) {
		fprintf(stderr, "face landmark initial failed.\n" );
		return -1;
	}

	ifstream seq_list(argv[2]);
	if (!seq_list.is_open()) {
		fprintf(stderr, "open imagelist failed.\n");
		return -1;
	}

	/* ============= train =============== */
	HeadGestureLearning *hgl_shake = new HeadGestureLearning(shake_transmt,shake_emismt,shake_pi);
	HeadGestureLearning *hgl_nod   = new HeadGestureLearning(nod_transmt, nod_emismt, nod_pi);

	hgl_shake->fetch_data(argv[3]);
	hgl_nod->fetch_data(argv[4]);

	hgl_shake->hmm_train(hmm_shake, MAX_ITER);
	hgl_nod->hmm_train(hmm_nod, MAX_ITER);

	// update parameters
	shake_transmt = hgl_shake->get_trans();
	shake_emismt  = hgl_shake->get_emis();
	shake_pi      = hgl_shake->get_pi();

	cout << "updated shake" << endl;
	print_matrix(shake_transmt);
        cout << endl;
	print_matrix(shake_emismt);
	
	/* shallow copy */
	nod_transmt   = hgl_nod->get_trans();
	nod_emismt    = hgl_nod->get_emis();
	nod_pi        = hgl_nod->get_pi();

	cout << "updated nod" << endl;
	print_matrix(nod_transmt);
        cout << endl;
	print_matrix(nod_emismt);

	/* =============  prediction ================= */
	cv::Mat detectRect      = cv::Mat(1,4,CV_32F);
	cv::Mat detectLandmarks = cv::Mat(51, 2,CV_32F);
	cv::Mat detectPose      = cv::Mat(1,3,CV_32F);
	cv::Mat rawImage;
	cv::Mat grayImage;
	std::vector<float> pitch;
	std::vector<float> yaw;
	string img_name;
	string line;
	int i = 0, j = 0;
	bool if_end = false;

    while (1) {
		for (j = i; j < 16+i; j++) {
			if (getline(seq_list, line)) {
				int pos = line.find_last_of("/");
				if (pos == -1) {
					img_name = string(argv[1])+"/"+line;
				} else {
					img_name = string(argv[1])+"/"+line.substr(pos+1,-1);
				}

				// read image
				rawImage = imread(img_name, CV_LOAD_IMAGE_COLOR);
				if (!rawImage.data) {
					fprintf(stderr, "Invalid input image!!!\n" );
					continue; // default value
				}
				cvtColor(rawImage, grayImage, COLOR_BGR2GRAY);
				// run face detection
				if (-1 == RunFaceDetection(&fdEngine,grayImage,detectRect)) {
					//fprintf(stderr, "no face detected!!!\n" );
					continue; // default value
				}

				// run face landmark
				if (-1 == RunFaceAlignmentSDM(&sdEngine,grayImage,detectRect,detectLandmarks)) {
					//fprintf(stderr, "face landmark failed!!!\n" );
					continue;  // default value
				}

				// run pose estimation
				if (-1 == RunPoseEstimate(detectLandmarks, detectPose)) {
					//fprintf(stderr, "pose estimate failed!!!\n" );
					continue;
				}

				// store features
				pitch.push_back(detectPose.at<float>(0,1));
				yaw.push_back(detectPose.at<float>(0,2));

			} else {
				if_end = true;
				break;
			}
		}

		// process over!
		if (if_end) {
			break;
		}

		// HMM 
		Mat obs_seq = Mat(1, pitch.size(), CV_32S);  // observed sequences during a period
		if (-1 == generate_seq(pitch, yaw, pitch.size(), obs_seq)) {
			fprintf(stderr, "generate sequence failed.\n" );
			return -1;
		}

		hmm_nod.decode(obs_seq, nod_transmt, nod_emismt, nod_pi, 
			           nod_logseq, p_nod_state, nod_fw, nod_bw);

		hmm_shake.decode(obs_seq, shake_transmt, shake_emismt, shake_pi,
			             shake_logseq, p_shake_state, shake_fw, shake_bw);

		cout << "nod logseq = " << nod_logseq << " , shake logseq = " << shake_logseq << endl;

		//if (MAX(nod_logseq, shake_logseq) > THRESHOLD) {
			if (nod_logseq > shake_logseq)
				cout << "Nodding" << endl;
			else 
				cout << "Shaking" << endl;
		//} else {
		//	cout << "Other Gesture" << endl;
		//}

		i += 16;
		pitch.clear();
		yaw.clear();
	}


	// free points
	delete hgl_shake;
	delete hgl_nod;
	hgl_shake = NULL;
	hgl_nod   = NULL;

	return 0;
}


///////////////////////////////////////////////////////////

/**
 * Descript: obtain parameters of hmm from file
 * @param hmm_model_path: (I) hmm model path
 * @param transmat: (O) transition probability matrix
 * @param emismat : (O) emission probability matrix
 * 
 * @return void
 *
 * @author tangwl
 * @date 2015/12/18
 *
 *                             file format
 *                 + + +;+ + +;+ + + ---->>>> transition matrix
 *                 + + + + +;+ + + + +;+ + + + +  ----->>>> emission matrix
 * 
 */
void parse_hmm_model(const char* hmm_model_path, Mat &trans_mat, Mat &emis_mat)
{
}


/**
 * Descript: map the feature vectors to observed sequence
 * @param pitch_vec : (I) pitch value vector of current frames
 * @param yaw_vec   : (I) yaw value vector of current frames
 * @param seq_len   : (I) length of sequence
 * @param obs_seq   : (O) output a sequence with the same length of feature vector ,
 *                        which is among 1~N
 * 
 * @return void
 *
 * @author tangwl
 * @date 2015/12/21
 */
int generate_seq(const vector<float> &pitch_vec, const vector<float> &yaw_vec, unsigned long seq_len, Mat &obs_seq)
{
	float still_pitch = 0.0f;
	float still_yaw   = 0.0f;
	float diff_pitch  = 0.0f;
	float diff_yaw    = 0.0f;

	if (seq_len <= 1)
		return -1;

	obs_seq.at<int>(0,0) = 4;

	for (int i = 1; i < seq_len; i++) {
		diff_pitch = pitch_vec[i] - pitch_vec[i-1];
		diff_yaw   = yaw_vec[i] - yaw_vec[i-1];

		if (abs(diff_yaw) <= THRESH_YAW && abs(diff_pitch) <= THRESH_PITCH) {
			obs_seq.at<int>(0,i) = 4;      // Still
			continue;
		}

		if (abs(diff_yaw) > abs(diff_pitch)) {
			if (diff_yaw > 0) {
				obs_seq.at<int>(0,i) = 1;  // Right
			} else {
				obs_seq.at<int>(0,i) = 0;  // Left
			} 
		} else {
			if (diff_pitch > 0) {
				obs_seq.at<int>(0,i) = 3;  // Down
			} else {
				obs_seq.at<int>(0,i) = 2;  // Up
			}
		}
	}

	return 0;
} 
