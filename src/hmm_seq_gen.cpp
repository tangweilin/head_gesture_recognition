/**
 * Automatically label the gesture sequences according 
 * to pose estimation
 * 
 * \date 2016.01.08
 * \ver  1.0
 *
 * ChangeLog: remove boost filesystem library 
 * \date 2016.01.11
 * \ver  1.1
**/

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <math.h>
#include <string>
#include <string.h>
#include <map>

#include "FaceDetection.h"
#include "FaceAlignmentSDM.h"
#include "PoseEstimate.h"

// #include "boost/filesystem.hpp"
#include "boost/algorithm/string.hpp"

#include "lsdir.hpp"  // added by tangwl 2016.01.11

using namespace std;
using namespace cv;

// namespace fs = boost::filesystem;

// face detection model path
static char cascadeFileName[256] = "./models/haarcascade_frontalface_alt2.xml";
static char shake_feature[256]   = "./results/shake_feature.txt";
static char nod_feature[256]     = "./results/nod_feature.txt";
static char shake_seq[256]       = "./results/shake_seq.txt";
static char nod_seq[256]         = "./results/nod_seq.txt";

// 给vector进行排序，按照Index
void make_order(vector<string> &v)
{
	map<int, string> m;
	vector<string> v_tmp;
	vector<string>::iterator iter;
	map<int, string>::iterator it;

	for (iter = v.begin(); iter != v.end(); iter++) {
		vector<string> tmp;
		boost::split(tmp, *iter, boost::is_any_of("_"));     // split by "_"
		// tmp includes {shake, 1, 1.jpg}
		boost::split(tmp, tmp.at(2), boost::is_any_of("."));

		m.insert(pair<int, string>(atoi(tmp.at(0).c_str()), *iter));
	}

	for (it = m.begin(); it != m.end(); ++it)
	{
		v_tmp.push_back(it->second);
	}

	v.assign(v_tmp.begin(), v_tmp.end());
	v_tmp.clear();
	m.clear();
}



//////////////////////////////////////////////

int main(int argc, char const *argv[])
{
	if (argc < 3)
	{
		/* return */
		fprintf(stderr, "usage: exe\tshake_dir\tnod_dir\n" );
		return -1;
	}

	// 遍历当前目录下的子目录
	// const string& shake_dir(argv[1]);
	// const string& nod_dir(argv[2]);

	// fs::path shake_path(shake_dir);
	// fs::path nod_path(nod_dir);

	// if (!fs::exists(shake_path) || (!fs::exists(nod_path))) {
	// 	cout << "no directory exists" << endl;
	// 	return -1;
	// }

	// fs::directory_iterator end_iter;

	vector<node> shake_images;
	vector<node> nod_images;
	bool lastSlash_0 = false;
	bool lastSlash_1 = false;

	if (argv[1][strlen(argv[1])-1] == '/')
		lastSlash_0 = true;
	if (argv[2][strlen(argv[2])-1] == '/')
		lastSlash_1 = true;

	listdir(argv[1], shake_images, lastSlash_0);
	listdir(argv[2], nod_images  , lastSlash_1);

	const string shake_whole_dir = string(argv[1]);
	const string nod_whole_dir   = string(argv[2]);

	map<int, string> image_lists;

	FDEngine fdEngine;
	SDEngine sdEngine;
	Mat rawImage, grayImage;
	Mat detectRect      = Mat(1,4,CV_32F);
	Mat detectLandmarks = Mat(51,2,CV_32F);
	Mat detectPose      = Mat(1,3,CV_32F);

	if (-1 == InitFaceDetection(&fdEngine, cascadeFileName)) {
		fprintf(stderr, "face detection initial failed\n" );
		return -1;
	}

	if (-1 == InitFaceAlignmentSDM(&sdEngine)) {
		fprintf(stderr, "face landmark initial failed.\n" );
		return -1;
	}

	ofstream shake_feature_ofs(shake_feature);
	ofstream nod_feature_ofs(nod_feature);
	ofstream shake_seq_ofs(shake_seq);
	ofstream nod_seq_ofs(nod_seq);

	if (!shake_feature_ofs.is_open() || (!nod_feature_ofs.is_open())) {
		fprintf(stderr, "open file for writing failed\n" );
		return -1;
	}


	// parse all files under each subdirectory
	vector<node>::iterator iter;
	vector<node>   single_names;

	for (iter = shake_images.begin(); iter != shake_images.end(); ++iter)
	{
		if (iter->type == "D") {
			// directory
			string subdir;
			if (lastSlash_0)
				subdir = shake_whole_dir + iter->name;
			else 
				subdir = shake_whole_dir + "/" + iter->name;

			listdir(subdir.c_str(), single_names, false);

			for (vector<node>::iterator it = single_names.begin(); it != single_names.end(); ++it)
			{
				vector<string> fields;
				boost::split(fields, it->name, boost::is_any_of("_"));
				boost::split(fields, fields.at(2), boost::is_any_of("."));
				image_lists.insert(pair<int, string>(atoi(fields.at(0).c_str()), it->name));
			}

			// extract features and map to sequences
			int   frame_idx  = 0;
			float prev_yaw   = 0.0f;

			map<int, string>::iterator it;

			for (it = image_lists.begin(); it != image_lists.end(); ++it)
			{
				string full_image_name = subdir + "/" + it->second;
                                cout << "process " << it->second << endl;

				rawImage = imread(full_image_name, CV_LOAD_IMAGE_COLOR);
				if (!rawImage.data) {
					fprintf(stderr, "%s: Invalid input image!!!\n", it->second.c_str());
					continue; // default value
				}
				cvtColor(rawImage, grayImage, COLOR_BGR2GRAY);
				if (-1 == RunFaceDetection(&fdEngine, grayImage, detectRect)) {
					fprintf(stderr, "%s: no face detected!!!\n", it->second.c_str());
					continue;
				}
				// run face landmark
				if (-1 == RunFaceAlignmentSDM(&sdEngine,grayImage,detectRect,detectLandmarks)) {
					fprintf(stderr, "%s: face landmark failed!!!\n", it->second.c_str() );
					continue;  // default value
				}

				// run pose estimation
				if (-1 == RunPoseEstimate(detectLandmarks, detectPose)) {
					fprintf(stderr, "%s: pose estimate failed!!!\n", it->second.c_str());
					continue;
				}

				// write data
				// format as:
				// xxx.jpg pitch yaw
				// pitch - nodding , yaw - shaking
				shake_feature_ofs << it->second << " " << detectPose.at<float>(0,1) << " " << detectPose.at<float>(0,2) << "\n";

				if (frame_idx == 0)
				{
					if (detectPose.at<float>(0,2) > 0) {
						shake_seq_ofs << 1 << " ";// right
					} else if (detectPose.at<float>(0,2) < 0 ) {
						// left
						shake_seq_ofs << 0 << " ";
					} else {
						// still
						shake_seq_ofs << 4 << " ";
					}

					// prev_pitch = detectPose.at<float>(0, 1);
					prev_yaw   = detectPose.at<float>(0, 2);

				} else {
					// float diff_pitch = detectPose.at<float>(0,1) - prev_pitch;
					float diff_yaw   = detectPose.at<float>(0,2) - prev_yaw;
					if (diff_yaw > 0) {
						shake_seq_ofs << 1 << " ";  /* turn right */
					} else if (diff_yaw < 0) {
						shake_seq_ofs << 0 << " ";  /* turn left */
					} else {
						shake_seq_ofs << 4 << " ";  /* keep still */
					}

					// prev_pitch = detectPose.at<float>(0,1);
					prev_yaw   = detectPose.at<float>(0,2);
				}

				frame_idx++;
			}
			shake_seq_ofs << "\n";

			image_lists.clear();
			single_names.clear();
		}
	}

	shake_feature_ofs.close();
	shake_seq_ofs.close();

        cout << "***************** begin to process nod sequences ******************" << endl;
	for (iter = nod_images.begin(); iter != nod_images.end(); ++iter) {
		if (iter->type == "D") {
			string subdir;
			if (lastSlash_1)
				subdir = nod_whole_dir + iter->name;
			else 
				subdir = nod_whole_dir + "/" + iter->name;

			listdir(subdir.c_str(), single_names, false);

			for (vector<node>::iterator it = single_names.begin(); it != single_names.end(); ++it)
			{
				vector<string> fields;
				boost::split(fields, it->name, boost::is_any_of("_"));
				boost::split(fields, fields.at(2), boost::is_any_of("."));
				image_lists.insert(pair<int, string>(atoi(fields.at(0).c_str()), it->name));
			}

			// extract features and mapping
			int frame_idx    = 0;
			float prev_pitch = 0.0f; 

			map<int, string>::iterator it;

			for (it = image_lists.begin(); it != image_lists.end(); ++it)
			{
				string full_image_name = subdir + "/" + it->second;
                                cout << "process " << it->second << endl;

				rawImage = imread(full_image_name, CV_LOAD_IMAGE_COLOR);
				if (!rawImage.data) {
					fprintf(stderr, "%s: Invalid input image!!!\n", it->second.c_str());
					continue; // default value
				}
				cvtColor(rawImage, grayImage, COLOR_BGR2GRAY);
				if (-1 == RunFaceDetection(&fdEngine, grayImage, detectRect)) {
					fprintf(stderr, "%s: no face detected!!!\n", it->second.c_str());
					continue;
				}
				// run face landmark
				if (-1 == RunFaceAlignmentSDM(&sdEngine,grayImage,detectRect,detectLandmarks)) {
					fprintf(stderr, "%s: face landmark failed!!!\n", it->second.c_str() );
					continue;  // default value
				}

				// run pose estimation
				if (-1 == RunPoseEstimate(detectLandmarks, detectPose)) {
					fprintf(stderr, "%s: pose estimate failed!!!\n", it->second.c_str());
					continue;
				}

				// write data
				// format as:
				// xxx.jpg pitch yaw
				nod_feature_ofs << it->second << " " << detectPose.at<float>(0,1) << " " << detectPose.at<float>(0,2) << "\n";

				if (frame_idx == 0) {
					if (detectPose.at<float>(0, 1) > 0) {
						nod_seq_ofs << 3 << " "; // Down
					} else if (detectPose.at<float>(0, 1) < 0 ) {
						nod_seq_ofs << 2 << " ";
					} else {
						// still
						nod_seq_ofs << 4 << " ";
					}

					prev_pitch   = detectPose.at<float>(0, 1);
				} else {
					// float diff_pitch = detectPose.at<float>(0,1) - prev_pitch;
					float diff_pitch   = detectPose.at<float>(0, 1) - prev_pitch;
					if (diff_pitch > 0) {
						nod_seq_ofs << 3 << " ";  /* turn down */
					} else if (diff_pitch < 0) {
						nod_seq_ofs << 2 << " ";  /* turn up  */
					} else {
						nod_seq_ofs << 4 << " ";  /* keep still */
					}

					prev_pitch = detectPose.at<float>(0,1);
				}

				frame_idx++;
			}

			nod_seq_ofs << "\n";
			image_lists.clear();
			single_names.clear();
		}
	}

	nod_feature_ofs.close();
	nod_seq_ofs.close();

	return 0;
}
