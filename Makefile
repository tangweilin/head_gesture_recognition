CC = gcc
CXX = g++

#TEST_SRC = ./src/head_gesture_recognition.cpp
SRC = ./src
TEST_SRC = $(wildcard $(SRC)/*.cpp)

# C optimization flags
COPT	= -O3 -funroll-loops -fomit-frame-pointer  -fno-tree-pre -falign-loops -ffast-math -ftree-vectorize -DLINUX

# C++ optimization flags
CXXOPT = $(COPT)

# C compilation flags
CFLAGS	= $(COPT) -Wall -Wextra -Wno-write-strings

# C++ compilation flags
CXXFLAGS	= $(CXXOPT) -Wall -Wextra -Wno-write-strings -Wno-deprecated

# link flags
LDFLAGS = -shared -Wl,--retain-symbols-file symbol -Wl,--version-script script

INCLUDES = -I ./include -I ./include/opencv -I ./include/opencv2

all:test_hg

%.o: %.c
	${CC} -c -fPIC ${CFLAGS} ${INCLUDES} $< -o $@

%.o: %.cpp
	${CXX} -c -fPIC ${CXXFLAGS} ${INCLUDES} $< -o $@

%.d: %.c
	${CC} -M ${INCLUDES} $< > $@

%.d: %.cpp
	${CXX} -M ${INCLUDES} $< > $@

test_hg:
	g++ -g $(TEST_SRC) $(INCLUDES) -Wl,-rpath,./ -L./ -lFaceDetection -lFaceAlignmentSDM -lPoseEstimate ./lib/libopencv_objdetect.a ./lib/libopencv_highgui.a ./lib/libopencv_imgproc.a ./lib/libopencv_core.a ./lib/libIlmImf.a ./lib/libpng.a ./lib/libjasper.a ./lib/libtiff.a ./lib/libjpeg.a ./lib/libzlib.a -lpthread -o $@

clean:
	rm -rf test_hg

